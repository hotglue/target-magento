"""Magento target class."""

from typing import Type

from singer_sdk import typing as th
from singer_sdk.sinks import Sink
from singer_sdk.target_base import Target

from target_magento.sinks import SalesOrdersSink,ProductSink

SINK_TYPES = [SalesOrdersSink,ProductSink]


class TargetMagento(Target):
    """Sample target for Magento."""

    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, parse_env_config, validate_config)

    name = "target-magento"
    config_jsonschema = th.PropertiesList(
        th.Property("username", th.StringType),
        th.Property("password", th.StringType),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "store_url", th.StringType, required=True, description="The store url"
        ),
    ).to_dict()

    def get_sink_class(self, stream_name: str) -> Type[Sink]:
        """Get sink for a stream."""
        return next(
            (
                sink_class
                for sink_class in SINK_TYPES
                if sink_class.unified_schema.Stream.name.lower() == stream_name.lower()
            ),
            None,
        )


if __name__ == "__main__":
    TargetMagento.cli()
