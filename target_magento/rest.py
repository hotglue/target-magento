"""MagentoSink target sink class, which handles writing streams."""

import re
from datetime import datetime
from typing import Any, Callable, Dict, List, Optional, cast

import backoff
import requests
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError


class Rest:

    timeout = 300
    access_token = None

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        headers["Content-Type"] = "application/json"
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")

        authenticator = self.authenticator
        if authenticator:
            headers.update({"Authorization": f"Bearer {authenticator}"} or {})
        return headers

    def get_token(self):
        if not self.access_token:
            s = requests.Session()
            payload = {
                "Content-Type": "application/json",
                "username": self.config.get("username"),
                "password": self.config.get("password"),
            }
            store_url = self.config["store_url"]
            if self.config["store_url"].endswith("/"):
                store_url = self.config["store_url"][:-1]

            login = s.post(
                f"{store_url}/index.php/rest/V1/integration/admin/token",
                json=payload,
            )
            if login.status_code >= 300:
                login = s.post(
                    f"{store_url}/rest/V1/integration/admin/token",
                    json=payload,
                )
            login.raise_for_status()

            self.access_token = login.json()

        return self.access_token

    @backoff.on_exception(
        backoff.expo,
        (RetriableAPIError, requests.exceptions.ReadTimeout),
        max_tries=5,
        factor=2,
    )
    def _request(
        self, http_method, endpoint, params=None, request_data=None
    ,url=None,headers=None) -> requests.PreparedRequest:
        """Prepare a request object."""
        if not url:
            url = self.url(endpoint)
        if not headers:
            headers = self.http_headers

        response = requests.request(
            method=http_method,
            url=url,
            params=params,
            headers=headers,
            json=request_data,
        )
        self.validate_response(response)
        return response

    def request_api(self, http_method, endpoint=None, params=None, request_data=None,headers=None):
        """Request records from REST endpoint(s), returning response records."""
        resp = self._request(http_method, endpoint, params, request_data,headers=headers)
        return resp

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if response.status_code in [429] or 500 <= response.status_code < 600:
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            try:
                msg = response.text
            except:
                msg = self.response_error_message(response)
            raise FatalAPIError(msg)

    def response_error_message(self, response: requests.Response) -> str:
        """Build error message for invalid http statuses."""
        if 400 <= response.status_code < 500:
            error_type = "Client"
        else:
            error_type = "Server"

        return (
            f"{response.status_code} {error_type} Error: "
            f"{response.reason} for path: {self.endpoint}"
        )

    @staticmethod
    def clean_dict_items(dict):
        return {k: v for k, v in dict.items() if v not in [None, ""]}

    def clean_payload(self, item):
        item = self.clean_dict_items(item)
        output = {}
        for k, v in item.items():
            if isinstance(v, datetime):
                dt_str = v.strftime("%Y-%m-%dT%H:%M:%S%z")
                if len(dt_str) > 20:
                    output[k] = f"{dt_str[:-2]}:{dt_str[-2:]}"
                else:
                    output[k] = dt_str
            elif isinstance(v, dict):
                output[k] = self.clean_payload(v)
            else:
                output[k] = v
        return output
