"""Magento target sink class, which handles writing streams."""
from hotglue_models_ecommerce.ecommerce import SalesOrder,Product

import json
from target_magento.client import MagentoSink


def map_line_items(items):
    lines = []

    if isinstance(items, str):
        items = json.loads(items)

    for line in items:
        lines.append({
            # "item_id": line.get("product_id"),
            "name": line.get("product_name"),
            "sku": line.get("sku"),
            "qty_invoiced": line.get("quantity"),
            "qty_ordered": line.get("quantity"),
            "base_original_price": line.get("unit_price"),
            "original_price": line.get("unit_price"),
            "base_original_price": line.get("unit_price"),
            "base_price": line.get("unit_price"),
            "base_row_invoiced": line.get("unit_price") * line.get("quantity"),
            "price": line.get("total_price"),
            "price_incl_tax": line.get("total_price"),
            "base_price_incl_tax": line.get("total_price"),
            "base_row_total": line.get("total_price"),
            "base_row_total_incl_tax": line.get("total_price"),
            "row_total": line.get("total_price"),
            "row_total_incl_tax": line.get("total_price"),
            "discount_amount": line.get("discount_amount"),
            "base_discount_amount": line.get("discount_amount"),
            "base_tax_amount": line.get("tax_amount"),
            "tax_amount": line.get("tax_amount"),
            "options": line.get("options")
        })

    return lines


def map_address(address, phone, customer_first_name, customer_last_name, address_type="billing"):
    if isinstance(address, str):
        address = json.loads(address)

    country_code = address.get("country")[:2].upper()

    address_obj = {
        "city": address.get("city"),
        "region": address.get("state"),
        "postcode": address.get("postal_code"),
        "country_id": country_code,
        "street": [address.get(key) for key in address.keys() if key.startswith("line")],
        "firstname": customer_first_name,
        "lastname": customer_last_name,
        "address_type": address_type
    }
    if phone:
        address_obj["telephone"] = phone

    return address_obj

class SalesOrdersSink(MagentoSink):
    """Magento target sink class."""

    endpoint = "/orders"
    unified_schema = SalesOrder

    def preprocess_record(self, record: dict, context: dict) -> dict:
        entity = {}
        # for now we only care about updating status based on id
        mapping = {}
        if record.get("id"):
            entity["entity_id"] = record.pop("id")

        first_name, *last_name = record.pop("customer_name", "").split(" ")
        last_name = " ".join(last_name)
        phone_number = record.pop("phone", None)
        entity["billing_address"] = map_address(
            record.pop("billing_address"),
            phone_number,
            first_name,
            last_name
        )
        entity["extension_attributes"] = {}
        if record.get("shipping_address"):
            shipping_addr_obj = {
                "address": map_address(
                    record.pop("shipping_address"),
                    phone_number,
                    first_name,
                    last_name,
                    "shipping"
                ),
            }
            if record.get("carrier"):
                shipping_addr_obj["method"] = record.pop("carrier")

            entity["extension_attributes"]["shipping_assignments"] = [{
                "shipping": shipping_addr_obj
            }]

        for key in [
            "status", "customer_id", "customer_email", "created_at", "updated_at"
        ]:
            if record.get(key):
                entity[key] = record[key]

        if record.get("line_items"):
            entity["items"] = map_line_items(record["line_items"])

        if "shipping_assignments" in entity["extension_attributes"]:
            if len(entity["extension_attributes"]["shipping_assignments"]) > 0:
                entity["extension_attributes"]["shipping_assignments"][0]["items"] = entity["items"]

        for item in entity.get("items", []):
            options = item.pop("options", []) or []
            for option in options:
                item["product_option"] = {
                    "extension_attributes": {}
                }
                option_type = option.get("type", "custom_options")
                if option_type not in item["product_option"]["extension_attributes"]:
                    item["product_option"]["extension_attributes"][option_type] = [
                        {
                            "option_id": option["id"],
                            "option_value": option["value"],
                        }
                    ]
                else:
                    item["product_option"]["extension_attributes"][option_type].append(
                        {
                            "option_id": option["id"],
                            "option_value": option["value"],
                        }
                    )

        entity["base_currency_code"] = record.pop("currency", None)
        entity["subtotal_incl_tax"] = record.get("subtotal", 0) + record.get("total_tax", 0)
        entity["base_subtotal_incl_tax"] = record.get("subtotal", 0) + record.get("total_tax", 0)
        entity["base_subtotal"] = record.get("subtotal", 0)
        entity["subtotal_incl_tax"] = record.get("subtotal", 0) + record.get("total_tax", 0)
        entity["subtotal"] = record.get("subtotal", 0)
        entity["base_total_due"] = record.get("total_price", 0)
        entity["total_due"] = record.get("total_price", 0)
        entity["base_grand_total"] = record.get("total_price", 0)
        entity["grand_total"] = record.get("total_price", 0)
        entity["discount_amount"] = record.get("total_discount", 0)
        entity["base_discount_amount"] = record.get("total_discount", 0)
        entity["payment"] = {
            "method": record.get("payment_method","cash"),
        }

        #Check for x_forward_value
        if record.get("custom_fields", []):
            if isinstance(record.get("custom_fields"), str):
                record["custom_fields"] = json.loads(record.get("custom_fields"))

            for field in record.get("custom_fields"):
                if field.get("name") == "x_forwarded_for":
                    entity["x_forwarded_for"] = field.get("value")

        if record.get("store_id"):
            store_id = record.pop("store_id")
            if isinstance(store_id, str):
                store_id = int(store_id)
            mapping["store_id"] = store_id
        mapping["entity"] = entity


        return self.validate_output(mapping)

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        method = "POST"
        action = "created"
        headers = self.http_headers

        # Perform update if id is present
        if record["entity"].get("entity_id"):
            id = record["entity"].get("entity_id")
            method = "POST"
            action = "updated"
            status = record["entity"].get("status").lower()
            if "cancelled" in status:
                method = "POST"
                self.endpoint = f"/orders/{record['entity'].get('entity_id')}/cancel"
                action = "cancelled"

            # check if order needs to be refunded

        response = self.request_api(
            method,
            self.endpoint,
            request_data=record,
            headers=headers
        )
        response = response.json()
        if isinstance(response, dict):
            id = response.get("entity_id")
            self.logger.info(f"{self.stream_name} {action} with id: {id}")
        else:
            self.logger.info(f"{self.stream_name} {action} with id: {id}")

class ProductSink(MagentoSink):

    endpoint = "/products"
    unified_schema = Product

    def preprocess_record(self, record: dict, context: dict) -> dict:
        record = self.validate_input(record)

        # Use the Default attributes set
        # (be sure to ask the client add the variants attributes to this attribute set)
        attr_id = 4

        # Check if the options in the product are contained in the attributes
        # return the necessary information to link the attributes to the products
        # if not, create new attributes and then return the information
        attrs = self.get_attributes(attr_id,record.get('options',None))

        # Get the categorie for the produc, if not found on categories will write a new category.
        # The search is done by name
        categorie = self.get_categories(record.get('category',None))

        # Create the parent product
        parent_product = {
            "sku" : record.get("sku",None),
            "name" : record.get("name",None),
            "price" : record.get("price",0.0),
            "status" : 1,
            "visibility" : 4,
            "type_id" : "configurable",
            "attribute_set_id" : attr_id,
            "weight" : record.get("weight",0.0),
            "extension_attributes": {
                        "category_links": [
                            {
                                "position": 0,
                                "category_id": categorie['id']
                            }
                        ]
                    },
        }

        # Append it to the bulk payload
        mapping = []
        mapping.append({'product':parent_product})


        variants = record.get('variants')
        if variants:
            for variant in variants:

                variant_product = {
                    "sku": variant.get("sku",None),
                    "name": record.get("name",None),
                    "price": variant.get("price",0.0),
                    "status":1,
                    "visibility":1,
                    "type_id": "simple",
                    "attribute_set_id":attr_id,
                    "weight":record.get("weight",0.0),
                    "extension_attributes": {
                        "category_links": [
                            {
                                "position": 0,
                                "category_id": categorie['id']
                            }
                        ],
                        "stock_item": {
                            "qty": str(variant.get('available_quantity','0')),
                            "is_in_stock": True
                            }
                    },
                }

                if attrs is not None:
                    variant_attributes = self.generate_custom_attributes(attrs,variant)
                    variant_product.update({"custom_attributes":variant_attributes})

                mapping.append({'product':variant_product})

        return mapping

    def generate_custom_attributes(self,attrs,variant):

        custom_attributes = []

        variant_options = variant.get('options',[])

        for opt in variant_options:
            # Getting the name of the custom_attribute
            opt_name = opt.get("name","")
            if opt_name in attrs:
                # Getting its possible values
                opt_values = attrs[opt_name].get('options')
                attribute_code = attrs[opt_name].get("attribute_code")
                for value in opt_values:
                    # Get the value from Magento associated with the value from
                    # the input
                    if opt['value'] == value['label']:
                        custom_attributes.append({
                            "attribute_code":attribute_code,
                            "value": value['value']
                        })

        return custom_attributes

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        method = "POST"
        url = f'{self.bulk_url}/products'

        response =  self._request(http_method=method,endpoint='', request_data=record,url=url)
        response = response.json()
        if isinstance(response, dict):
            for item in response.get('request_items',[]):
                id = item.get("id")
                status = item.get("status")
                self.logger.info(f"{self.stream_name} creation status: {status} with id: {id}")

    def query_attributes_sets(self,attribute_set_name)->bool:
        '''This method filters the attributes_sets to find one
        with 'attribute_set_name' == attribute_set_name'''

        # Query parameters
        params = {
            'searchCriteria[filter_groups][0][filters][0][field]':'attribute_set_name',
            'searchCriteria[filter_groups][0][filters][0][value]':attribute_set_name,
            'searchCriteria[filter_groups][0][filters][0][condition_type]':'eq'
        }
        method = 'GET'
        endpoint = '/eav/attribute-sets/list'
        request = self._request(http_method = method,
                    endpoint=endpoint,
                    params=params)

        items = request.json().get('items')

        if items:
            return items[0]['attribute_set_id']
        return False

    def get_categories(self,category_name):
        '''This method filters the attributes_sets to find one
        with 'attribute_set_name' == attribute_set_name'''

        # Query parameters
        params = {
            'searchCriteria[filter_groups][0][filters][0][field]':'id',
            'searchCriteria[filter_groups][0][filters][0][value]':1,
            'searchCriteria[filter_groups][0][filters][0][condition_type]':'gte'
        }
        method = 'GET'
        endpoint = '/categories'
        request = self._request(http_method = method,
                    endpoint=endpoint,
                    params=params)

        categories = request.json().get('children_data')
        categories =  {item['name']:item for item in categories}

        if category_name is not None:
            if category_name['name'] in categories:
                return categories[category_name['name']]
        return categories['Default Category']

    def get_attributes(self,attr_id,attr_list):
        # If no attributes are in the Parent product, then no attributes will
        # be passed to the variants
        if attr_list is None:
           return None

        # If there are, check if they aleready exist in the list of attributes from
        # Hotglue attributes set
        # Get the attributes from Hotglue's attributes set
        attributes = self.attributes_from_set(attr_id)

        # Check if the attributes in the list are in Hotglue
        product_attr = {}
        for attr in attr_list:
            if attr.lower() in attributes:
                product_attr.update({attr:attributes[attr.lower()]})

        return product_attr

    def create_attribute():
        return

    def attributes_from_set(self,attr_id):
        '''This method return the attributes from
        the attribute_set with the attr_id'''

        method = 'GET'
        endpoint = f'/products/attribute-sets/{attr_id}/attributes'
        request = self._request(http_method = method,
                    endpoint=endpoint)

        attributes = request.json()

        return {item['attribute_code']:item for item in attributes}