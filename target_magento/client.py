"""MagentoSink target sink class, which handles writing streams."""

from typing import Any, Callable, Dict, List, Optional, cast

from singer_sdk.sinks import RecordSink
from singer_sdk.plugin_base import PluginBase

from target_magento.rest import Rest

from oauthlib.oauth1 import SIGNATURE_HMAC_SHA256
from requests_oauthlib import OAuth1

class MagentoSink(RecordSink, Rest):
    """MagentoSink target sink class."""

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        """Initialize target sink."""
        self._target = target
        self.attributes_sets = None
        super().__init__(target, stream_name, schema, key_properties)

    @property
    def authenticator(self):
        """Return a new authenticator object."""
        if self.config.get("username") and self.config.get("password") is not None:
            token = self.get_token()
        elif self.config.get("use_oauth", False):
            token = self.config.get("oauth_token", self.config.get("access_token"))
        else:
            token = self.config.get("api_key")
        return token

    api_version = "V1"

    @property
    def name(self):
        raise NotImplementedError

    @property
    def endpoint(self):
        raise NotImplementedError

    @property
    def unified_schema(self):
        raise NotImplementedError

    @property
    def base_url(self):
        return f"{self.config.get('store_url')}/rest/all/V1"

    @property
    def bulk_url(self):
        return f"{self.config.get('store_url')}/rest/default/async/bulk/V1"

    def url(self, endpoint=None):
        if not endpoint:
            endpoint = self.endpoint

        return f"{self.base_url}{endpoint}"

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()

    def validate_output(self, mapping):
        payload = self.clean_payload(mapping)
        # Add validation logic here
        return payload


    def prepare_request(self, context, next_page_token):
        request = super().prepare_request(context, next_page_token)

        if self.config.get("use_oauth"):
            request.auth = OAuth1(
                self.config.get('consumer_key'),
                client_secret=self.config.get('consumer_secret'),
                resource_owner_key=self.config.get("oauth_token", self.config.get("access_token")),
                resource_owner_secret=self.config.get("oauth_token_secret", self.config.get("access_token_secret")),
                signature_type="auth_header",
                signature_method=SIGNATURE_HMAC_SHA256
            )
        
        return request

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        response = self.request_api("POST", request_data=record)
        id = response.json().get("id")
        self.logger.info(f"{self.name} created with id: {id}")
